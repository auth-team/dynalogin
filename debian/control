Source: dynalogin
Section: admin
Priority: optional
Maintainer: Debian Authentication Maintainers <pkg-auth-maintainers@lists.alioth.debian.org>
Uploaders: Daniel Pocock <daniel@pocock.com.au>
Build-Depends: cdbs, debhelper (>= 8.0.0), libapr1-dev, liboath-dev, unixodbc-dev, autotools-dev, pkg-config, libgnutls28-dev, libpam-dev
Homepage: http://www.dynalogin.org/
Standards-Version: 3.9.3
Vcs-Git: git://git.debian.org/pkg-auth/dynalogin.git
Vcs-Browser: http://git.debian.org/?p=pkg-auth/dynalogin.git;a=summary

Package: libdynalogin-1-0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: two-factor HOTP/TOTP authentication - implementation libs
 dynalogin is a two-factor authentication framework based on HOTP/TOTP
 (Open Authentication) algorithms.  Dynalogin can store credentials in
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This library is used by a dynalogin authentication server or any other
 software component that wants to implement the HOTP/TOTP algorithm
 internally.  The package also includes various storage modules (file
 storage and UNIXODBC) for retaining the user credentials and algorithm
 data.
 
Package: dynalogin-server
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: two-factor HOTP/TOTP authentication - server daemon
 dynalogin is a two-factor authentication framework based on the HOTP/TOTP
 (Open Authentication) algorithms.  Dynalogin can store credentials in 
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This package provides dynalogind, the server daemon for a dynalogin
 two-factor authentication server.

Package: libdynaloginclient-1-0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: two-factor HOTP/TOTP authentication - implementation libs
 dynalogin is a two-factor authentication framework based on the HOTP/TOTP
 (Open Authentication) algorithms.  Dynalogin can store credentials in
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This library is used by any application that wants to authenticate
 users against the dynalogin server.  It is able to make the TLS
 connection to the dynalogin server and communicate requests and
 responses using the dynalogin wire protocol.  It is used by
 pam_dynalogin.

Package: libpam-dynalogin
Section: admin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: two-factor HOTP/TOTP authentication - implementation libs
 dynalogin is a two-factor authentication framework based on the HOTP/TOTP
 (Open Authentication) algorithms.  Dynalogin can store credentials in
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This PAM module allows authentication against a dynalogin server
 anywhere in the network.

Package: dynalogin-client-php
Section: web
Architecture: all
Depends: ${misc:Depends}
Description: two-factor HOTP/TOTP authentication - PHP client
 dynalogin is a two-factor authentication framework based on the HOTP/TOTP
 (Open Authentication) algorithm.  Dynalogin can store credentials in 
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This package provides a PHP client for the dynalogin server.  It allows
 PHP applications to validate a user login attempt.  It is intended as a
 foundation for login forms, OpenID providers and similar code.

Package: simpleid-store-dynalogin
Section: web
Architecture: all
Depends: ${misc:Depends}, simpleid, dynalogin-client-php
Description: two-factor HOTP/TOTP authentication - OpenID provider
 dynalogin is a two-factor authentication framework based on the HOTP/TOTP
 (Open Authentication) algorithm.  Dynalogin can store credentials in
 any database supported by UNIXODBC, which makes the solution
 robust and scalable.  It can also store credentials in flat files
 if desired.  dynalogin has been successfully integrated in solutions
 for OpenID, making it possible to use two-factor authentication with
 hundreds of other web applications and public web sites.  There is a
 dynalogin soft-token for Android and it also works with Google
 Authenticator.
 .
 This package provides a datastore extension for the OpenID provider
 SimpleID, which is implemented entirely in PHP and has minimal other
 dependencies.  The extension allows SimpleID users (configured in flat
 files) to authenticate against a dynalogin server.

